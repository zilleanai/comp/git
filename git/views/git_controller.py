# source: https://gist.github.com/fallenhitokiri/9f3b87ccb80f420ae81cd9820d74dca5
import os
import json
from flask import current_app, make_response, request, jsonify, abort, send_file
from flask_unchained import BundleConfig, Controller, route, injectable
from backend.config import Config as AppConfig
from io import StringIO, BytesIO
from dulwich.pack import PackStreamReader
import subprocess
import zipfile
import uuid
import subprocess
from git import Repo
from git.exc import InvalidGitRepositoryError

from werkzeug.utils import secure_filename
from ..tasks import pull_async_task, commit_async_task


class GitController(Controller):

    @route('/<string:project_name>/info/refs')
    def info_refs(self, project_name):
        service = request.args.get('service')
        if service[:4] != 'git-':
            abort(500)
        p = subprocess.Popen([service, '--stateless-rpc', '--advertise-refs',
                              os.path.join(AppConfig.DATA_FOLDER, project_name)], stdout=subprocess.PIPE)
        packet = '# service=%s\n' % service
        length = len(packet) + 4
        prefix = "{:04x}".format(length & 0xFFFF)
        data = prefix + packet + '0000'
        data += p.stdout.read().decode('utf-8')
        res = make_response(data)
        res.headers['Expires'] = 'Fri, 01 Jan 1980 00:00:00 GMT'
        res.headers['Pragma'] = 'no-cache'
        res.headers['Cache-Control'] = 'no-cache, max-age=0, must-revalidate'
        res.headers['Content-Type'] = 'application/x-%s-advertisement' % service
        p.wait()
        return res

    @route('/<string:project_name>/git-receive-pack', methods=['GET', 'POST'])
    def git_receive_pack(self, project_name):
        p = subprocess.Popen(['git-receive-pack', '--stateless-rpc', os.path.join(
            AppConfig.DATA_FOLDER, project_name)], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
        data_in = request.data
        p.stdin.write(data_in)
        p.stdin.close()
        data_out = p.stdout.read()
        res = make_response(data_out)
        res.headers['Expires'] = 'Fri, 01 Jan 1980 00:00:00 GMT'
        res.headers['Pragma'] = 'no-cache'
        res.headers['Cache-Control'] = 'no-cache, max-age=0, must-revalidate'
        res.headers['Content-Type'] = 'application/x-git-receive-pack-result'
        p.wait()
        return res

    @route('/<string:project_name>/git-upload-pack', methods=['GET', 'POST'])
    def git_upload_pack(self, project_name):
        p = subprocess.Popen(['git-upload-pack', '--no-strict', '--stateless-rpc', os.path.join(
            AppConfig.DATA_FOLDER, project_name)], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        p.stdin.write(request.data)
        p.stdin.close()
        data = p.stdout.read()
        res = make_response(data)
        res.headers['Expires'] = 'Fri, 01 Jan 1980 00:00:00 GMT'
        res.headers['Pragma'] = 'no-cache'
        res.headers['Cache-Control'] = 'no-cache, max-age=0, must-revalidate'
        res.headers['Content-Type'] = 'application/x-git-upload-pack-result'
        p.wait()
        return res

    @route('/status/<string:project>')
    def status(self, project):
        BASE_DIR = os.path.join(
            BundleConfig.current_app.config.DATA_FOLDER, project)
        if not os.path.exists(BASE_DIR):
            return abort(404)
        try:
            repo = Repo(BASE_DIR)
        except InvalidGitRepositoryError as e:
            return abort(404)
        changedFiles = [ item.a_path for item in repo.index.diff(None) ]

        return jsonify(project=project, untracked=repo.untracked_files, changed=changedFiles)

    @route('/log/<string:project>')
    def log(self, project):
        BASE_DIR = os.path.join(
            BundleConfig.current_app.config.DATA_FOLDER, project)
        if not os.path.exists(BASE_DIR):
            return abort(404)
        repo = Repo(BASE_DIR)
        branch = repo.head.reference
        log = branch.log()
        return jsonify(project=project, log=log)

    @route('/commit/<string:project>', methods=['POST'])
    def commit(self, project):
        BASE_DIR = os.path.join(
            BundleConfig.current_app.config.DATA_FOLDER, project)
        if not os.path.exists(BASE_DIR):
            return abort(404)
        message = "'" + request.json['message'] + "'"
        commit_async_task.delay(project, message)
        return jsonify(success=True)

    @route('/remote/<string:project>', methods=['POST'])
    def set_remote(self, project):
        BASE_DIR = os.path.join(
            BundleConfig.current_app.config.DATA_FOLDER, project)
        if not os.path.exists(BASE_DIR):
            return abort(404)
        remote = request.json['remote']
        repo = Repo(BASE_DIR)
        repo.create_remote('origin', url=remote)
        return jsonify(success=True)

    @route('/pull/<string:project>', methods=['POST'])
    def pull(self, project):
        BASE_DIR = os.path.join(
            BundleConfig.current_app.config.DATA_FOLDER, project)
        if not os.path.exists(BASE_DIR):
            return abort(404)
        pull_async_task.delay(project)
        return jsonify(success=True)

    @route('/ssh/<string:project>')
    def ssh_pub(self, project):
        BASE_DIR = os.path.join(
            AppConfig.SSH_FOLDER, project)
        if not os.path.exists(BASE_DIR):
            return abort(404)
        with open(BASE_DIR+'.pub', 'r') as file:
            data = file.read().replace('\n', '')
        return jsonify(project=project, ssh_pub=data)
