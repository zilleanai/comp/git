import os
from flask_unchained.bundles.celery import celery
from backend.config import Config as AppConfig

from git import Repo


@celery.task(serializer='json')
def pull_async_task(project):
    BASE_DIR = os.path.join(
        AppConfig.DATA_FOLDER, project)
    ssh_path = os.path.join(AppConfig.SSH_FOLDER, project)
    if not os.path.exists(BASE_DIR):
        return
    repo = Repo(BASE_DIR)
    with repo.git.custom_environment(GIT_SSH_COMMAND='ssh -o StrictHostKeyChecking=no -v -i ' + ssh_path):
        repo.remotes.origin.pull()


@celery.task(serializer='json')
def commit_async_task(project, message):
    BASE_DIR = os.path.join(
        AppConfig.DATA_FOLDER, project)
    ssh_path = os.path.join(AppConfig.SSH_FOLDER, project)
    if not os.path.exists(BASE_DIR):
        return
    repo = Repo(BASE_DIR)
    repo.git.config('--local', 'user.email', 'zillean@localhost')
    repo.git.config('--local', 'user.name', 'zillean')
    repo.index.add(repo.untracked_files)
    try:
        repo.git.commit('-a',  '-m', message)
    except Exception as e:
        print(e)
    with repo.git.custom_environment(GIT_SSH_COMMAND='ssh -o StrictHostKeyChecking=no -v -i ' + ssh_path):
        repo.git.push('--set-upstream', 'origin', 'master')
