import { call, put, select, takeEvery, takeLatest } from 'redux-saga/effects'

import { createRoutineSaga } from 'sagas'
import { convertDates } from 'utils'

import { gitStatus } from 'comps/git/actions'
import GitApi from 'comps/git/api'
import { selectGitStatus } from 'comps/git/reducers/gitstatus'


export const KEY = 'gitStatus'

export const maybeLoadGitStatusSaga = function* (payload) {
  const { byProject, isLoading } = yield select(selectGitStatus)
  const isLoaded = !!byProject[payload.project]
  if (!(isLoaded || isLoading)) {
    yield put(gitStatus.trigger(payload))
  }
}

export const loadGitStatusSaga = createRoutineSaga(
  gitStatus,
  function* successGenerator({ project }) {
    const status = yield call(GitApi.gitStatus, { project })
    yield put(gitStatus.success({ status }))
  }
)

export default () => [
  takeEvery(gitStatus.MAYBE_TRIGGER, maybeLoadGitStatusSaga),
  takeLatest(gitStatus.TRIGGER, loadGitStatusSaga),
]
