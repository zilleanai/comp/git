import { call, put, takeLatest } from 'redux-saga/effects'

import { gitCommit } from '../actions'
import { createRoutineFormSaga } from 'sagas'
import GitApi from '../api'


export const KEY = 'git_commit'

export const gitCommitSaga = createRoutineFormSaga(
  gitCommit,
  function* successGenerator(payload) {
    const response = yield call(GitApi.gitCommit, payload)
    yield put(gitCommit.success(response))
  }
)

export default () => [
  takeLatest(gitCommit.TRIGGER, gitCommitSaga)
]
