import { call, put, takeLatest } from 'redux-saga/effects'

import { createRoutineSaga } from 'sagas'
import { gitPull } from 'comps/git/actions'
import GitApi from 'comps/git/api'


export const KEY = 'gitPull'

export const gitPullSaga = createRoutineSaga(
  gitPull,
  function *successGenerator(payload) {
    payload = {}
    const response = yield call(GitApi.gitPull, payload)
    yield put(gitPull.success(response))}
)

export default () => [
  takeLatest(gitPull.TRIGGER, gitPullSaga),
]
