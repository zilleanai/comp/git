import { call, put, takeLatest } from 'redux-saga/effects'

import { gitSetRemote } from '../actions'
import { createRoutineFormSaga } from 'sagas'
import GitApi from '../api'


export const KEY = 'git_set_remote'

export const gitSetRemoteSaga = createRoutineFormSaga(
  gitSetRemote,
  function* successGenerator(payload) {
    const response = yield call(GitApi.gitSetRemote, payload)
    yield put(gitSetRemote.success(response))
  }
)

export default () => [
  takeLatest(gitSetRemote.TRIGGER, gitSetRemoteSaga)
]
