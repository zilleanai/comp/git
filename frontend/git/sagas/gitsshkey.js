import { call, put, select, takeEvery, takeLatest } from 'redux-saga/effects'

import { createRoutineSaga } from 'sagas'

import { gitSSHKey} from 'comps/git/actions'
import GitApi from 'comps/git/api'
import { selectGitSSHKey } from 'comps/git/reducers/gitsshkey'


export const KEY = 'gitSSHKey'

export const maybeLoadGitSSHKeySaga = function* (payload) {
  const { byProject, isLoading } = yield select(selectGitSSHKey)
  const isLoaded = !!byProject[payload.project]
  if (!(isLoaded || isLoading)) {
    yield put(gitSSHKey.trigger(payload))
  }
}

export const loadGitSSHKeySaga = createRoutineSaga(
  gitSSHKey,
  function* successGenerator({ project }) {
    const ssh = yield call(GitApi.gitSSHKey, { project })
    yield put(gitSSHKey.success({ ssh }))
  }
)

export default () => [
  takeEvery(gitSSHKey.MAYBE_TRIGGER, maybeLoadGitSSHKeySaga),
  takeLatest(gitSSHKey.TRIGGER, loadGitSSHKeySaga),
]
