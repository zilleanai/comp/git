import { get, post } from 'utils/request'
import { v1 } from 'api'
import { storage } from 'comps/project'

function git(uri) {
    return v1(`/git${uri}`)
}

export default class Git {
    static gitStatus({ project }) {
        return get(git(`/status/${project}`))
    }

    static gitCommit({ message }) {
        return post(git(`/commit/${storage.getProject()}`), { 'message': message })
    }
    static gitSetRemote({ remote }) {
        return post(git(`/remote/${storage.getProject()}`), { 'remote': remote })
    }
    static gitPull() {
        return post(git(`/pull/${storage.getProject()}`))
    }
    static gitSSHKey({ project }) {
        return get(git(`/ssh/${project}`))
    }
}
