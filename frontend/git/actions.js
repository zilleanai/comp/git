import { createRoutine } from 'actions'
export const gitStatus = createRoutine('git/GIT_STATUS')
export const gitCommit = createRoutine('git/GIT_COMMIT')
export const gitPull = createRoutine('git/GIT_PULL')
export const gitSetRemote = createRoutine('git/GIT_SET_REMOTE')
export const gitSSHKey = createRoutine('git/GIT_SSH_KEY')
