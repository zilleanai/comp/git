import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import formActions from 'redux-form/es/actions'
const { reset } = formActions
import { bindRoutineCreators } from 'actions'
import { injectSagas } from 'utils/async'
import { TextField } from 'components/Form'
import { gitCommit } from 'comps/git/actions'
import './commit.scss'
const FORM_NAME = 'gitCommit'

class Commit extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
    }
  }

  render() {
    const { error, handleSubmit, pristine, submitting } = this.props

    return (
      <div className='commit'>
        <form onSubmit={handleSubmit(gitCommit)}>
          <TextField name='message' />
          <div className="row">
            <button type="submit"
              className="button-primary"
              disabled={pristine || submitting}
            >
              {submitting ? 'Committing...' : 'Commit'}
            </button>
          </div>
        </form>
      </div>
    )
  }
}

const withConnect = connect(
  (state, props) => {
    return {
    }
  },
  (dispatch) => bindRoutineCreators({}, dispatch),
)


const withForm = reduxForm({
  form: FORM_NAME,
  onSubmitSuccess: (_, dispatch) => {
    dispatch(reset(FORM_NAME))
  }
})

const withSaga = injectSagas(require('comps/git/sagas/gitcommit'))

export default compose(
  withSaga,
  withForm,
  withConnect,
)(Commit)
