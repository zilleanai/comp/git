import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import formActions from 'redux-form/es/actions'
const { reset } = formActions
import { bindRoutineCreators } from 'actions'
import { injectSagas } from 'utils/async'
import { gitPull } from 'comps/git/actions'
import './pull.scss'
const FORM_NAME = 'gitPull'

class Pull extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
    }
  }

  render() {
    const { error, handleSubmit, pristine, submitting, gitPull } = this.props

    return (
      <div className='pull'>
        <button type="button"
              className="button-primary"
              onClick={()=>{gitPull.trigger()}}>
        Pull
        </button>
      </div>
    )
  }
}

const withConnect = connect(
  (state, props) => {
    return {
    }
  },
  (dispatch) => bindRoutineCreators({gitPull}, dispatch),
)


const withForm = reduxForm({
  form: FORM_NAME,
  onSubmitSuccess: (_, dispatch) => {
    dispatch(reset(FORM_NAME))
  }
})

const withSaga = injectSagas(require('comps/git/sagas/gitpull'))

export default compose(
  withSaga,
  withForm,
  withConnect,
)(Pull)
