import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { bindRoutineCreators } from 'actions'
import { injectReducer, injectSagas } from 'utils/async'
import { selectGitSSHKeyByProject } from 'comps/git/reducers/gitsshkey'
import { gitSSHKey } from 'comps/git/actions'
import { storage } from 'comps/project'
import './ssh-key.scss'

class SSHKey extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
    }
  }

  componentWillMount() {
    this.props.gitSSHKey.trigger({ project: storage.getProject() })
  }

  render() {
    const { error, isLoaded, ssh } = this.props
    if(!isLoaded){
      return null
    }
    return (
      <div className='ssh-key'>
      <label>Public SSH Key</label>
      <textarea className='ssh-pub' value={ssh.ssh_pub}></textarea>
      
      </div>
    )
  }
}

const withReducer = injectReducer(require('comps/git/reducers/gitsshkey'))
const withSaga = injectSagas(require('comps/git/sagas/gitsshkey'))

const withConnect = connect(
  (state, props) => {
    const ssh = selectGitSSHKeyByProject(state, storage.getProject())
    const isLoaded = !!ssh
    return {
      isLoaded,
      ssh
    }
  },
  (dispatch) => bindRoutineCreators({ gitSSHKey }, dispatch),
)

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(SSHKey)
