import { gitSSHKey } from 'comps/git/actions'


export const KEY = 'gitsshkey'

const initialState = {
  isLoading: false,
  isLoaded: false,
  projects: [],
  byProject: {},
  error: null,
}

export default function (state = initialState, action) {
  const { type, payload } = action
  const { ssh } = payload || {}
  const { projects, byProject } = state

  switch (type) {
    case gitSSHKey.REQUEST:
      return {
        ...state,
        isLoading: true,
      }

    case gitSSHKey.SUCCESS:
      const project = ssh.project
      if (!projects.includes(project)) {
        projects.push(project)
      }
      byProject[project] = ssh
      return {
        ...state,
        ssh: ssh,
        projects,
        byProject,
        isLoaded: true,
      }

    case gitSSHKey.FAILURE:
      return {
        ...state,
        error: payload.error,
      }


    case gitSSHKey.FULFILL:
      return {
        ...state,
        isLoading: false,
      }

    default:
      return state
  }
}

export const selectGitSSHKey = (state) => state[KEY]
export const selectGitSSHKeyByProject = (state, project) => {
  const status = selectGitSSHKey(state).byProject[project]
  return status
}