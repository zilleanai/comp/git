import { gitStatus } from 'comps/git/actions'


export const KEY = 'gitstatus'

const initialState = {
  isLoading: false,
  isLoaded: false,
  projects: [],
  byProject: {},
  error: null,
}

export default function (state = initialState, action) {
  const { type, payload } = action
  const { status } = payload || {}
  const { projects, byProject } = state

  switch (type) {
    case gitStatus.REQUEST:
      return {
        ...state,
        isLoading: true,
      }

    case gitStatus.SUCCESS:
      const project = status.project
      if (!projects.includes(project)) {
        projects.push(project)
      }
      byProject[project] = status
      return {
        ...state,
        status: status,
        projects,
        byProject,
        isLoaded: true,
      }

    case gitStatus.FAILURE:
      return {
        ...state,
        error: payload.error,
      }


    case gitStatus.FULFILL:
      return {
        ...state,
        isLoading: false,
      }

    default:
      return state
  }
}

export const selectGitStatus = (state) => state[KEY]
export const selectGitStatusByProject = (state, project) => {
  const status = selectGitStatus(state).byProject[project]
  return status
}