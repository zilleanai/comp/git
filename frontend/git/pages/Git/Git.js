import React from 'react'
import Helmet from 'react-helmet'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { bindRoutineCreators } from 'actions'
import { injectReducer, injectSagas } from 'utils/async'
import { PageContent } from 'components'
import { Steps } from 'intro.js-react';
import 'intro.js/introjs.css';
import { selectGitStatusByProject } from 'comps/git/reducers/gitstatus'
import { gitStatus } from 'comps/git/actions'
import { storage } from 'comps/project'
import { Commit, Pull, Remote, SSHKey } from '../../components'

class Git extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      stepsEnabled: true,
      initialStep: 0,
      steps: [
        {
          element: '.untracked',
          intro: 'List of untracked files. Untracked files are files which are not under version control.',
        },
        {
          element: '.changed',
          intro: 'List of changed files. These files are under version control. They where changed and the changes were not added to version control yet.',
        },
        {
          element: '.commit',
          intro: 'When commiting you add untracked and changed files to version control. A message should be given with description what has changed or was added.',
        },
        {
          element: '.pull',
          intro: 'Pull here to get latest version of the project if changed in git project.',
        },
        {
          element: '.remote',
          intro: 'Set remote to the git project where you want commit to. This should be starting with git@.',
        },
        {
          element: '.ssh-key',
          intro: 'Paste the public ssh key to your git project to allow the platform to pull and push. (https://docs.gitlab.com/ee/ssh/#per-repository-deploy-keys)',
        },
      ],
    }
  }

  componentWillMount() {
    this.props.gitStatus.trigger({ project: storage.getProject() })
  }

  render() {
    const { isLoaded, error, status } = this.props
    const { stepsEnabled, steps, initialStep } = this.state;
    if (!isLoaded) {
      return null
    }
    const { untracked, changed } = status
    return (
      <PageContent>
        <Steps
          enabled={stepsEnabled}
          steps={steps}
          initialStep={initialStep}
          onExit={() => {
            this.setState(() => ({ stepsEnabled: false }));
          }}
        />
        <Helmet>
          <title>Git</title>
        </Helmet>
        <h5>untracked files</h5>
        <ul className="untracked" >
          <div>
            {untracked.map((filename, i) => {
              return (
                <li key={i}>
                  {filename}
                </li>
              )
            }
            )}
          </div>
        </ul>
        <h5>changed files</h5>
        <ul className="changed" >
          <div>
            {changed.map((filename, i) => {
              return (
                <li key={i}>
                  {filename}
                </li>
              )
            }
            )}
          </div>
        </ul>
        <Commit />
        <Pull />
        <Remote />
        <SSHKey />
      </PageContent>
    )
  }
}

const withReducer = injectReducer(require('comps/git/reducers/gitstatus'))
const withSaga = injectSagas(require('comps/git/sagas/gitstatus'))

const withConnect = connect(
  (state, props) => {
    const status = selectGitStatusByProject(state, storage.getProject())
    const isLoaded = !!status
    return {
      isLoaded,
      status
    }
  },
  (dispatch) => bindRoutineCreators({ gitStatus }, dispatch),
)

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(Git)
